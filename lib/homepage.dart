import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passTextEditingController = TextEditingController();

  Future<void> convertToEmailAuth(
      String email, String password, User user) async {
    try {
      AuthCredential emailCredential =
          EmailAuthProvider.credential(email: email, password: password);
      await user.linkWithCredential(emailCredential);
      debugPrint(
          'Anonymous user converted to email authentication: ${user.uid}');
    } catch (e) {
      debugPrint('Error converting to email authentication: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: auth.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            User? user = snapshot.data;
            if (user == null) {
              // User is signed out
              return const Text('Not signed in');
            } else {
              // User is signed in
              return Center(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  user.email == null
                      ? Column(
                          children: [
                            Text('Signed in as: ${user.uid}'),
                            ElevatedButton(
                                onPressed: () async {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        content: Column(
                                          children: [
                                            TextFormField(
                                              controller:
                                                  emailTextEditingController,
                                            ),
                                            TextFormField(
                                              controller:
                                                  passTextEditingController,
                                            ),
                                          ],
                                        ),
                                        actions: [
                                          ElevatedButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: const Text("Cancel"),
                                          ),
                                          ElevatedButton(
                                              onPressed: () async {
                                                await convertToEmailAuth(
                                                        emailTextEditingController
                                                            .text,
                                                        passTextEditingController
                                                            .text,
                                                        user)
                                                    .then((value) {
                                                  Navigator.pop(context);
                                                  setState(() {});
                                                });
                                              },
                                              child: const Text("Sign In"))
                                        ],
                                      );
                                    },
                                  );
                                },
                                child: const Text(
                                    "Convert To Email Authentication"))
                          ],
                        )
                      : Text('Signed in as: ${user.email}'),
                ],
              ));
            }
          }
          return const Text('Loading...');
        },
      ),
    );
  }
}
